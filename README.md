# COhpy DoJo Ramblings for April 30, 2020

The project for this week's DoJo ramblings contains a Jupyter notebook
containing code snippits discuss during the meeting.  Some of the snippits
require Python 3.8 to run.

The details of the DoJo meetings can be found in Meetup.  Look for the
Central Ohio Python User Group DoJo.  Currently they are being held online
so anyone can join the meeting.

The general web site for the group is at http://www.cohpy.org/.

